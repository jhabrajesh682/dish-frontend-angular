import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchPipe } from './common/pipe/search.pipe';
import { FontAwesomeModule } from 'ngx-icons';
import { HttpClientModule } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PipeModule } from './common/pipe/pipe.module';
import { LoginComponent } from './login/login.component';
import { ToastrModule } from 'ngx-toastr';


import { SidebarModule } from 'ng-sidebar';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DishComponent } from './dish/dish.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    DishComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    SidebarModule.forRoot(),
    NgbModule,
    NgMultiSelectDropDownModule,
    FontAwesomeModule,
    PipeModule,
    HttpClientModule,
    ToastrModule.forRoot()
  ],
  providers: [SearchPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
