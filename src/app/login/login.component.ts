import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ToastrService } from "ngx-toastr";
import { SearchPipe } from '../common/pipe/search.pipe';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  loginObj: any = {}
  filterSearch: string;
  registerObj: any = {}
  page = 1
  pageSize = 10
  data: any = []
  rows: any[] = []
  temp: any[] = []
  rowData: any[] = [];
  modalId: any[] = [];
  constructor(private http: HttpService, private toast: ToastrService,
    private route: Router) { }

  ngOnInit() {
  }

  login() {

    let finalObj = {
      Email: this.loginObj.email,
      password: this.loginObj.password
    }
    this.http.login(finalObj).subscribe(
      (resp) => {
        console.log("resp=====>", resp);
        let jwttoken = resp.token
        localStorage.setItem("authToken", jwttoken);
        console.log("token=====>", resp.token);
        console.log("successfully logged in")
        this.route
          .navigate([`../dish/`])
      },
      (err) => {
        console.log(err);
        this.toast.info("Pls provide correct credentials ")

      })
  }

  signup() {
    let signupObj = {
      Name: this.registerObj.name,
      roleName: this.registerObj.Addrole,
      Email: this.registerObj.addEmail,
      username: this.registerObj.addusername,
      password: this.registerObj.addPassword
    }
    this.http.signup(signupObj).subscribe(
      (resp) => {
        console.log("successfully user created")
        document.getElementById("registerModal").click();
      },
      (err) => {
        console.log(err);
        this.toast.info("Pls provide correct credentials ")

      })

  }

}
