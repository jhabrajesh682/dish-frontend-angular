import { Component, OnInit } from '@angular/core';
import { SearchPipe } from '../common/pipe/search.pipe';
import { HttpService } from '../services/http.service';
@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.scss']
})
export class DishComponent implements OnInit {
  dishObj: any = {}
  filterSearch: string;
  page = 1
  Files: any;
  dishId: string
  updatedFile: string;
  SASUrl: string
  pageSize = 10
  data: any = []

  rows: any[] = []
  temp: any[] = []
  fileId: string
  dishFile: string
  filechange: any;
  fileName: string;
  fileObj: any = {}
  constructor(private http: HttpService,
    private searchPipe: SearchPipe) { }

  ngOnInit() {
    this.getAllDish()
  }


  search() {
    if (this.filterSearch.length == 0) {
      this.rows = this.temp;
      return;
    } else {
      this.rows = this.searchPipe.transform(this.temp, this.filterSearch);
    }
  }
  fileChange(event) {

    this.Files = <File>event.target.files[0];

    console.log(this.Files.name)
    console.log(this.Files)
    this.fileName = this.Files.type.slice(0, 5);
  }

  getAllDish() {
    this.http.getAllDish().subscribe(data => {

      console.log('DATA in dish=======>🛫🛫 ', data);


      let dishArr = []


      data.dish.map(element => {

        dishArr.push(
          {

            'dishName': element.dishName,
            'file': element.photo.orignalName,
            'blobname': element.photo.blobName,
            'createdAt': element.createdAt,
            'dishId': element._id,
            'ingredientsName': element.ingredientsName,
            'ingredientsQuantity': element.ingredientsQuantity,
            'ingredientsUnit': element.ingredientsUnit,
            'steps': element.steps
          })
      })
      this.rows = dishArr
      this.temp = dishArr
      console.log("data in task rows===>🎌🎌 ", this.rows);


    }, err => {
      console.log(err);
    })
  }
  addModalData() {
    this.dishObj.addName = ''
    this.dishObj.addIngredientsName = ''
    this.dishObj.addQuantity = ''
    this.dishObj.addUnit = ''
    this.dishObj.addSteps = ''
    this.dishObj.addPicture = ''
  }

  editModalData(rowData) {
    console.log(rowData);
    this.dishId = rowData.dishId
    this.dishObj.updateName = rowData.dishName,
      this.dishObj.updateIngredientsName = rowData.ingredientsName,
      this.dishObj.updateQuantity = rowData.ingredientsQuantity
    this.dishObj.updateUnit = rowData.ingredientsUnit
    this.dishObj.updateSteps = rowData.steps
    this.updatedFile = rowData.file

    this.dishObj.updatePicture = rowData.file
    console.log('logo file================>', this.dishObj.updatePicture);
    this.Files = ''
    this.filechange = 'No Change';
    this.fileName = ''
  }

  getDishId(rowData) {

    this.dishId = rowData.dishId
    console.log("rowData==========>", this.dishId);

  }
  updatefileChange(event) {
    this.Files = '';
    this.filechange = 'Change'

    this.Files = <File>event.target.files[0];

    console.log(this.Files.name)
    console.log(this.Files)
    this.fileName = this.Files.type.slice(0, 5);
  }

  getUrl(rowData) {

    this.dishFile = rowData.blobname
    console.log("rowData==========>", this.dishFile);

    this.http.getDishSASUrl(this.dishFile).subscribe(
      (resp) => {
        this.SASUrl = resp.url;
      },
      (error) => {
        console.log(error);
      })
  }

  AddDish() {

    const formData = new FormData();
    formData.append('dishName', this.dishObj.addName);
    formData.append('ingredientsName', this.dishObj.addIngredientsName.toString());
    formData.append('ingredientsQuantity', this.dishObj.addQuantity);
    formData.append('ingredientsUnit', this.dishObj.addUnit);
    formData.append('steps', this.dishObj.addSteps);
    formData.append('orignalName', this.Files, this.Files.name);

    this.http.addDish(formData).subscribe(
      (resp) => {

        document.getElementById("addDish").click();
        this.getAllDish();
      },
      (err) => {
        console.log(err);

      })
  }

  updateDish() {

    const updateformData = new FormData();

    updateformData.append('dishName', this.dishObj.updateName);
    updateformData.append('ingredientsName', this.dishObj.updateIngredientsName);
    updateformData.append('ingredientsQuantity', this.dishObj.updateQuantity);
    updateformData.append('ingredientsUnit', this.dishObj.updateUnit);
    updateformData.append('steps', this.dishObj.updateSteps);
    if (this.filechange == 'No Change') {
      updateformData.append('orignalName', 'No Change');
    }
    if (this.filechange == 'Change') {
      updateformData.append('orignalName', this.Files, this.Files.name);
    }

    this.http.updateDish(updateformData, this.dishId).subscribe(
      (resp) => {

        document.getElementById("updateDish").click();
        this.getAllDish();
      },
      (err) => {
        console.log(err);

      })
  }

  deleteDish() {

    this.http.deleteDish(this.dishId).subscribe(
      (resp) => {
        document.getElementById("deleteDish").click();
        this.getAllDish()
      },
      (error) => {
        console.log(error);
      })
  }

}
